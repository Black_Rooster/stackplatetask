/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plate;

import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author senzo
 */
public class Stack {

    private List<String> stack = new ArrayList<>();
    private int top;

    public void Push(String x) {
        stack.add(x);
    }

    public String Pop() throws Exception {

        if (stack.isEmpty()) {
            throw new Exception("Sorry no more plates to be used.");
        }

        top = stack.size() - 1;
        String popValue = stack.get(top);
        stack.remove(top);

        return popValue;
    }

    public void Diplay()
    {
        System.out.println(stack);
    }
}
