/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plate;

import java.util.Scanner;

/**
 *
 * @author senzo
 */
public class Plate {

    /**
     * @param args the command line arguments
     */
    private static Stack plates = new Stack();
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {

        LoadPlate();

        try {
            Dish();
        } catch (Exception ex) {
            System.err.println(ex.getMessage() + "\nThe application is ending.......");
        }
    }

    private static void LoadPlate() {

        String plateName = "";

        while (!plateName.equalsIgnoreCase("done")) {
            System.out.println("Please enter a plate name");
            plateName = input.nextLine();

            if (!plateName.equalsIgnoreCase("done")) {
                plates.Push(plateName);
                plates.Diplay();
            }
        }
    }

    private static void Dish() throws Exception {

        String personName = "";
        String plateName;

        while (!personName.equalsIgnoreCase("done")) {
            System.out.println("Please enter a person name");
            personName = input.nextLine();

            if (!personName.equalsIgnoreCase("done")) {
                plateName = plates.Pop();
                System.out.println(plateName + " will be used by " + personName);
            }
        }
        
        plates.Diplay();
    }
}
